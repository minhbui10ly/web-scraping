import requests
from bs4 import BeautifulSoup
import multiprocessing as mp
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import database as db


def request_retry_session(
        retries=3,
        back_off_factor=0.3,
        status_force_list=(500, 502, 504),
        session=None,
):
    """
    This function makes request to the links if it not response
    :param retries: Retry times
    :param back_off_factor: Time increase after the second try
    :param status_force_list: A set of status code force to retry
    :param session: Initialize variable session
    :return: A session
    """
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=back_off_factor,
        status_forcelist=status_force_list,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def get_link(link_page, link_queue):
    """
    This function get link and put to queue
    :param link_page: page link
    :param link_queue: link queue
    """
    print('start get_link')
    last_link = False
    session = request_retry_session()

    while not last_link:
        response = session.get(link_page, allow_redirects=False)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, "lxml")
            urls = soup.select(".meta-news>.count_cmt")
            for link in urls:
                print(link['href'])
                link_queue.put(link['href'])
            # link_page = "stop"
            link_page = "https://vnexpress.net/" + soup.select_one(".next-page")['href']
        else:
            link_queue.put('stop')
            last_link = True
            print("stop get link")


def get_data(link, data_queue):
    """
    This function gets data from the website
    :param link: Url of the article
    :param data_queue: Put data to queue
    """

    print('start get data')
    print(link)

    if link == 'stop':
        data_queue.put('stop')
        return 0

    session = request_retry_session()
    response = session.get(link)
    soup = BeautifulSoup(response.text, "lxml")
    try:
        title = soup.select_one(".title-detail").get_text()
    except AttributeError:
        title = ""

    record = {'title': title,
              'description': soup.select_one(".description").get_text(),
              'url': link}

    data_queue.put(record)


def run_multiprocess():
    """
    This function makes program executing faster
    """

    link_page = 'https://vnexpress.net/giao-duc'

    manager = mp.Manager()
    data_queue = manager.Queue()
    link_queue = manager.Queue()
    pool = mp.Pool(22)
    stop = False
    pool.apply_async(get_link, (link_page, link_queue))

    while not stop:
        link = link_queue.get()
        if link == 'stop':
            stop = True
        pool.apply_async(get_data, (link, data_queue))

    pool.apply_async(db.insert_data, (data_queue, ))

    pool.close()
    pool.join()


if __name__ == '__main__':
    run_multiprocess()

