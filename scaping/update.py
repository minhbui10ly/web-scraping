import requests
from bs4 import BeautifulSoup
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import database


def request_retry_session(
        retries=3,
        back_off_factor=0.3,
        status_force_list=(500, 502, 504),
        session=None,
):
    """
    This function makes request to the links if it not response
    :param retries: Retry times
    :param back_off_factor: Time increase after the second try
    :param status_force_list: A set of status code force to retry
    :param session: Initialize variable session
    :return: A session
    """
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=back_off_factor,
        status_forcelist=status_force_list,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def update_data():
    """
    This function update data to database
    """
    print('start update')

    link = 'https://vnexpress.net/giao-duc'

    session = request_retry_session()
    response = session.get(link)
    soup = BeautifulSoup(response.text, 'lxml')
    for link in soup.select('.meta-news>.count_cmt'):
        if database.check_link(link) is False:
            try:
                title = soup.select_one(".title-detail").get_text()
            except AttributeError:
                title = ""

            record = {
                'title': title,
                'description': soup.select_one(".description").get_text(),
                'url': link
            }

            database.update_data(record)

