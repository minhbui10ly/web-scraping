beautifulsoup4==4.9.0
bs4==0.0.1
certifi==2019.11.28
chardet==3.0.4
DateTime==4.3
idna==2.8
lxml==4.5.0
numpy==1.18.2
pandas==1.0.3
peewee==3.13.1
python-dateutil==2.8.1
pytz==2019.3
requests==2.22.0
scipy==1.4.1
six==1.14.0
soupsieve==2.0
urllib3==1.25.8
zope.interface==5.1.0
