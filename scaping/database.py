from peewee import *


def connect_db():
    """
    This function make a connect to the localhost
    :return: Connect to the localhost
    """
    db = MySQLDatabase(
        'scrape_web',
        host="localhost",
        user="root",
        port=3306,
        passwd=""
    )
    return db


class VnExpress(Model):
    title = CharField()
    description = CharField()
    url = CharField()

    class Meta:
        database = connect_db()


def insert_data(data_queue):
    """
    This function creates and inserts data to the database
    :param data_queue: data queue
    """
    db = connect_db()
    db.connect()
    db.create_tables([VnExpress], safe=True)
    stop = False
    while not stop or not data_queue.empty():
        data = data_queue.get()
        if data == 'stop':
            stop = True
            print("stop insert")
        else:
            VnExpress.insert(data).execute()

    db.close()


def check_link(link):
    """
    This function check if link in database or not
    :param link: new link
    :return A bool value confirm if link exist or not
    """
    db = connect_db()
    db.connect()
    db.create_tables([VnExpress], safe=True)

    query = VnExpress.select().where(VnExpress.url == f"{link}")
    val = query.exists()
    if val is True:
        return True
    else:
        return False


def update_data(data):
    """
    This function update data
    :param data: insert new data to database
    """
    db = connect_db()
    db.connect()
    db.create_tables([VnExpress], safe=True)
    VnExpress.insert(data).execute()
    db.close()

